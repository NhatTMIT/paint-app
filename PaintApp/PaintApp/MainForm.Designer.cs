﻿namespace PaintApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbonUpDown1 = new System.Windows.Forms.RibbonUpDown();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.btnNew = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnOpen = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnSave = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnSaveAs = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnExit = new System.Windows.Forms.RibbonOrbMenuItem();
            this.TabHome = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.btnSelectTool = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.btnLine = new System.Windows.Forms.RibbonButton();
            this.btnRectangle = new System.Windows.Forms.RibbonButton();
            this.btnEllipse = new System.Windows.Forms.RibbonButton();
            this.btnPolygon = new System.Windows.Forms.RibbonButton();
            this.btnCurve = new System.Windows.Forms.RibbonButton();
            this.btnPencil = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.ribbonLabel1 = new System.Windows.Forms.RibbonLabel();
            this.txtSize = new System.Windows.Forms.RibbonTextBox();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.btnColor1 = new System.Windows.Forms.RibbonColorChooser();
            this.btnColor2 = new System.Windows.Forms.RibbonColorChooser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelDraw = new System.Windows.Forms.Panel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnBorderColor = new System.Windows.Forms.Button();
            this.btnBackGroundColor = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbShapeName = new System.Windows.Forms.Label();
            this.txtRotate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBorderWidth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonUpDown1
            // 
            this.ribbonUpDown1.TextBoxText = "";
            this.ribbonUpDown1.TextBoxWidth = 50;
            // 
            // ribbon1
            // 
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.btnNew);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.btnOpen);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.btnSave);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.btnSaveAs);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.btnExit);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 292);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.ribbon1.OrbText = "FILE";
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon1.Size = new System.Drawing.Size(1022, 143);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.TabHome);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // btnNew
            // 
            this.btnNew.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnNew.Image = global::PaintApp.Properties.Resources.File_32;
            this.btnNew.SmallImage = global::PaintApp.Properties.Resources.File_32;
            this.btnNew.Text = "New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnOpen.Image = global::PaintApp.Properties.Resources.Open_Folder_28;
            this.btnOpen.SmallImage = global::PaintApp.Properties.Resources.Open_Folder_28;
            this.btnOpen.Text = "Open";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnSave.Image = global::PaintApp.Properties.Resources.Save_28;
            this.btnSave.SmallImage = global::PaintApp.Properties.Resources.Save_28;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnSaveAs.Image = global::PaintApp.Properties.Resources.Save_as_Filled_28;
            this.btnSaveAs.SmallImage = global::PaintApp.Properties.Resources.Save_as_Filled_28;
            this.btnSaveAs.Text = "Save as";
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnExit
            // 
            this.btnExit.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnExit.Image = global::PaintApp.Properties.Resources.Exit_32;
            this.btnExit.SmallImage = global::PaintApp.Properties.Resources.Exit_32;
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // TabHome
            // 
            this.TabHome.Panels.Add(this.ribbonPanel3);
            this.TabHome.Panels.Add(this.ribbonPanel4);
            this.TabHome.Panels.Add(this.ribbonPanel5);
            this.TabHome.Panels.Add(this.ribbonPanel6);
            this.TabHome.Text = "Home";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.btnSelectTool);
            this.ribbonPanel3.Text = "Tool";
            // 
            // btnSelectTool
            // 
            this.btnSelectTool.CheckedGroup = "1";
            this.btnSelectTool.CheckOnClick = true;
            this.btnSelectTool.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectTool.Image")));
            this.btnSelectTool.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnSelectTool.SmallImage = global::PaintApp.Properties.Resources.Cursor_20;
            this.btnSelectTool.Text = "ribbonButton23";
            this.btnSelectTool.Click += new System.EventHandler(this.btnSelectTool_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.btnLine);
            this.ribbonPanel4.Items.Add(this.btnRectangle);
            this.ribbonPanel4.Items.Add(this.btnEllipse);
            this.ribbonPanel4.Items.Add(this.btnPolygon);
            this.ribbonPanel4.Items.Add(this.btnCurve);
            this.ribbonPanel4.Items.Add(this.btnPencil);
            this.ribbonPanel4.Text = "Shapes";
            // 
            // btnLine
            // 
            this.btnLine.CheckedGroup = "1";
            this.btnLine.CheckOnClick = true;
            this.btnLine.Image = ((System.Drawing.Image)(resources.GetObject("btnLine.Image")));
            this.btnLine.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnLine.SmallImage = global::PaintApp.Properties.Resources.Line_16;
            this.btnLine.Text = "ribbonButton27";
            this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
            // 
            // btnRectangle
            // 
            this.btnRectangle.CheckedGroup = "1";
            this.btnRectangle.CheckOnClick = true;
            this.btnRectangle.Image = ((System.Drawing.Image)(resources.GetObject("btnRectangle.Image")));
            this.btnRectangle.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnRectangle.SmallImage = global::PaintApp.Properties.Resources.Rectangle_Stroked_16;
            this.btnRectangle.Text = "ribbonButton29";
            this.btnRectangle.Click += new System.EventHandler(this.btnRectangle_Click);
            // 
            // btnEllipse
            // 
            this.btnEllipse.CheckedGroup = "1";
            this.btnEllipse.CheckOnClick = true;
            this.btnEllipse.Image = ((System.Drawing.Image)(resources.GetObject("btnEllipse.Image")));
            this.btnEllipse.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnEllipse.SmallImage = global::PaintApp.Properties.Resources.Circle_Thin_16;
            this.btnEllipse.Text = "ribbonButton30";
            this.btnEllipse.Click += new System.EventHandler(this.btnEllipse_Click);
            // 
            // btnPolygon
            // 
            this.btnPolygon.CheckedGroup = "1";
            this.btnPolygon.CheckOnClick = true;
            this.btnPolygon.Image = ((System.Drawing.Image)(resources.GetObject("btnPolygon.Image")));
            this.btnPolygon.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnPolygon.SmallImage = global::PaintApp.Properties.Resources.Polygon_Filled_16;
            this.btnPolygon.Text = "ribbonButton36";
            this.btnPolygon.Click += new System.EventHandler(this.btnPolygon_Click);
            // 
            // btnCurve
            // 
            this.btnCurve.CheckedGroup = "1";
            this.btnCurve.CheckOnClick = true;
            this.btnCurve.Image = ((System.Drawing.Image)(resources.GetObject("btnCurve.Image")));
            this.btnCurve.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnCurve.SmallImage = global::PaintApp.Properties.Resources.Creek_16;
            this.btnCurve.Text = "ribbonButton37";
            this.btnCurve.Click += new System.EventHandler(this.btnCurve_Click);
            // 
            // btnPencil
            // 
            this.btnPencil.CheckedGroup = "1";
            this.btnPencil.CheckOnClick = true;
            this.btnPencil.Image = ((System.Drawing.Image)(resources.GetObject("btnPencil.Image")));
            this.btnPencil.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnPencil.SmallImage = global::PaintApp.Properties.Resources.Edit_20;
            this.btnPencil.Text = "";
            this.btnPencil.Click += new System.EventHandler(this.btnPencil_Click);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.ribbonLabel1);
            this.ribbonPanel5.Items.Add(this.txtSize);
            this.ribbonPanel5.Text = "";
            // 
            // ribbonLabel1
            // 
            this.ribbonLabel1.Text = "Size";
            this.ribbonLabel1.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            // 
            // txtSize
            // 
            this.txtSize.Text = "";
            this.txtSize.TextBoxText = "3";
            this.txtSize.TextBoxWidth = 30;
            this.txtSize.TextBoxTextChanged += new System.EventHandler(this.ribbonTextBox1_TextBoxTextChanged);
            this.txtSize.TextBoxKeyDown += new System.Windows.Forms.KeyEventHandler(this.ribbonTextBox1_TextBoxKeyDown);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.Items.Add(this.btnColor1);
            this.ribbonPanel6.Items.Add(this.btnColor2);
            this.ribbonPanel6.Text = "Color";
            // 
            // btnColor1
            // 
            this.btnColor1.Color = System.Drawing.Color.White;
            this.btnColor1.Image = ((System.Drawing.Image)(resources.GetObject("btnColor1.Image")));
            this.btnColor1.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnColor1.SmallImage")));
            this.btnColor1.Text = "Back Ground Color";
            this.btnColor1.Click += new System.EventHandler(this.btnColor1_Click);
            // 
            // btnColor2
            // 
            this.btnColor2.Color = System.Drawing.Color.Black;
            this.btnColor2.Image = ((System.Drawing.Image)(resources.GetObject("btnColor2.Image")));
            this.btnColor2.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnColor2.SmallImage")));
            this.btnColor2.Text = "Border Color";
            this.btnColor2.Click += new System.EventHandler(this.btnColor2_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.panelDraw);
            this.panel1.Location = new System.Drawing.Point(0, 140);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 438);
            this.panel1.TabIndex = 1;
            // 
            // panelDraw
            // 
            this.panelDraw.BackColor = System.Drawing.Color.White;
            this.panelDraw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDraw.Location = new System.Drawing.Point(0, 0);
            this.panelDraw.Name = "panelDraw";
            this.panelDraw.Size = new System.Drawing.Size(1000, 541);
            this.panelDraw.TabIndex = 0;
            this.panelDraw.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            this.panelDraw.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelDraw_MouseClick);
            this.panelDraw.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panelDraw_MouseDoubleClick);
            this.panelDraw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelDraw_MouseDown);
            this.panelDraw.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelDraw_MouseMove);
            this.panelDraw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelDraw_MouseUp);
            this.panelDraw.Resize += new System.EventHandler(this.panel2_Resize);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnBorderColor);
            this.panel3.Controls.Add(this.btnBackGroundColor);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.lbShapeName);
            this.panel3.Controls.Add(this.txtRotate);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtBorderWidth);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtHeight);
            this.panel3.Controls.Add(this.txtWidth);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Enabled = false;
            this.panel3.Location = new System.Drawing.Point(797, 140);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(225, 438);
            this.panel3.TabIndex = 2;
            // 
            // btnBorderColor
            // 
            this.btnBorderColor.Location = new System.Drawing.Point(113, 238);
            this.btnBorderColor.Name = "btnBorderColor";
            this.btnBorderColor.Size = new System.Drawing.Size(75, 23);
            this.btnBorderColor.TabIndex = 12;
            this.btnBorderColor.UseVisualStyleBackColor = true;
            this.btnBorderColor.Click += new System.EventHandler(this.btnBorderColor_Click);
            // 
            // btnBackGroundColor
            // 
            this.btnBackGroundColor.Location = new System.Drawing.Point(113, 198);
            this.btnBackGroundColor.Name = "btnBackGroundColor";
            this.btnBackGroundColor.Size = new System.Drawing.Size(75, 23);
            this.btnBackGroundColor.TabIndex = 11;
            this.btnBackGroundColor.UseVisualStyleBackColor = true;
            this.btnBackGroundColor.Click += new System.EventHandler(this.btnBackGroundColor_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Border Color:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Background Color:";
            // 
            // lbShapeName
            // 
            this.lbShapeName.AutoSize = true;
            this.lbShapeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShapeName.Location = new System.Drawing.Point(27, 19);
            this.lbShapeName.Name = "lbShapeName";
            this.lbShapeName.Size = new System.Drawing.Size(0, 13);
            this.lbShapeName.TabIndex = 8;
            this.lbShapeName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtRotate
            // 
            this.txtRotate.Location = new System.Drawing.Point(102, 166);
            this.txtRotate.Name = "txtRotate";
            this.txtRotate.Size = new System.Drawing.Size(100, 20);
            this.txtRotate.TabIndex = 7;
            this.txtRotate.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtRotate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRotate_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Rotate:";
            // 
            // txtBorderWidth
            // 
            this.txtBorderWidth.Location = new System.Drawing.Point(102, 130);
            this.txtBorderWidth.Name = "txtBorderWidth";
            this.txtBorderWidth.Size = new System.Drawing.Size(100, 20);
            this.txtBorderWidth.TabIndex = 5;
            this.txtBorderWidth.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtBorderWidth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBorderWidth_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Border Width:";
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(102, 93);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(100, 20);
            this.txtHeight.TabIndex = 3;
            this.txtHeight.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHeight_KeyDown);
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(102, 55);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(100, 20);
            this.txtWidth.TabIndex = 2;
            this.txtWidth.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtWidth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWidth_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Height:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 577);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ribbon1);
            this.Name = "MainForm";
            this.Text = "Paint App";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RibbonUpDown ribbonUpDown1;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab TabHome;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonButton btnSelectTool;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton btnLine;
        private System.Windows.Forms.RibbonButton btnRectangle;
        private System.Windows.Forms.RibbonButton btnEllipse;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonColorChooser btnColor1;
        private System.Windows.Forms.RibbonButton btnPolygon;
        private System.Windows.Forms.RibbonButton btnCurve;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.RibbonOrbMenuItem btnNew;
        private System.Windows.Forms.RibbonOrbMenuItem btnOpen;
        private System.Windows.Forms.RibbonOrbMenuItem btnSave;
        private System.Windows.Forms.RibbonOrbMenuItem btnSaveAs;
        private System.Windows.Forms.RibbonOrbMenuItem btnExit;
        private System.Windows.Forms.Panel panelDraw;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBorderWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.TextBox txtRotate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbShapeName;
        private System.Windows.Forms.Button btnBorderColor;
        private System.Windows.Forms.Button btnBackGroundColor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RibbonButton btnPencil;
        private System.Windows.Forms.RibbonColorChooser btnColor2;
        private System.Windows.Forms.RibbonLabel ribbonLabel1;
        private System.Windows.Forms.RibbonTextBox txtSize;
    }
}

