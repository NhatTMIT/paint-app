﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;

namespace PaintApp.Business
{
    class BusinessRectangle
    {
        private PointF _firstPoint;
        public PointF FirstPoint
        {
            get { return this._firstPoint; }
            set { this._firstPoint = value; }
        }

        private PointF _secondPoint;
        public PointF SecondPoint
        {
            get { return this._secondPoint; }
            set 
            { 
                if (_rect != null)
                    _rect.PointList[1] = value;
                this._secondPoint = value;
            }
        }

        private PaintApp.Class.Rectangle _rect;
        public PaintApp.Class.Rectangle Rect
        {
            get { return this._rect; }
            set { this._rect = value; }
        }

        public BusinessRectangle() { }

        public BusinessRectangle(PaintApp.Class.Rectangle rect)
        {
            FirstPoint = rect.PointList[0];
            SecondPoint = rect.PointList[1];
            this._rect = rect;
        }


        private Class.Rectangle createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            this._rect = new PaintApp.Class.Rectangle(FirstPoint, SecondPoint, backgroundColor, borderColor, borderWidth);
            this._rect = new Class.Rectangle(FirstPoint, SecondPoint, borderColor, backgroundColor, borderWidth);
            return this._rect;
        }

        public Class.Rectangle mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            FirstPoint = firstPoint;
            SecondPoint = firstPoint;
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        public void mouseMove(Point secondPoint)
        {
            SecondPoint = secondPoint;
        }
    }
}
