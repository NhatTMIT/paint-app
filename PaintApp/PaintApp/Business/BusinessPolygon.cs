﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;

namespace PaintApp.Business
{
    class BusinessPolygon
    {
        private PointF _nextPoint;
        public PointF NextPoint
        {
            get { return this._nextPoint; }
            set 
            {
                if (this._polygon != null)
                    this._polygon.PointList.Add(value);
                this._nextPoint = value; 
            }
        }

        private PointF _currentPoint;
        public PointF CurrentPoint
        {
            get { return this._currentPoint; }
            set { this._currentPoint = value; }
        }

        private Class.Polygon _polygon;
        public Class.Polygon POLYGON
        {
            get { return this._polygon; }
            set { this._polygon = value; }
        }

        private bool _isDone = false;
        public bool IsDone
        {
            get { return _isDone; }
            set { this._isDone = value; }
        }

        private bool isFirst = false;

        public BusinessPolygon() 
        {}

        public BusinessPolygon(Class.Polygon polygon)
        {
            this._polygon = polygon;
        }

        public Class.Polygon createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            if (this._polygon != null)
            {
                return null;
            }
            else
            {
                this._polygon = new Class.Polygon(_nextPoint, borderColor, backgroundColor, borderWidth);
                isFirst = true;
                NextPoint = NextPoint;
                return this._polygon;
            }
            
        }

        public Class.Polygon mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            if (isFirst)
            {
                this._polygon.PointList[this._polygon.PointList.Count - 1] = firstPoint;
                isFirst = false;
            }
            else
                NextPoint = firstPoint;
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        public void mouseMove(Point secondPoint)
        {
            isFirst = false;
            this._polygon.PointList[this._polygon.PointList.Count - 1] = secondPoint;
        }

        public void mouseDoubleClick(Point point)
        {
            IsDone = true;
        }
    }
}
