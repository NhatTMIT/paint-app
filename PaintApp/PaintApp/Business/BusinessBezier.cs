﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaintApp.Class;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace PaintApp.Business
{
    class BusinessBezier
    {
        
        private PointF _nextPoint;
        public PointF NextPoint
        {
            get { return this._nextPoint; }
            set 
            {
                if (this._bezierCurve != null)
                {
                    this._bezierCurve.PointList.Add(value);
                    this._bezierCurve.PointList.Add(value);
                    this._bezierCurve.PointList.Add(value);
                }
                this._nextPoint = value; 
            }
        }

        private PointF _currentPoint;
        public PointF CurrentPoint
        {
            get { return this._currentPoint; }
            set { this._currentPoint = value; }
        }

        private Class.BezierCurve _bezierCurve;
        public Class.BezierCurve BEZIER
        {
            get { return this._bezierCurve; }
            set { this._bezierCurve = value; }
        }

        private bool _isDone = false;
        public bool IsDone
        {
            get { return _isDone; }
            set { this._isDone = value; }
        }

        public BusinessBezier() 
        {}

        public BusinessBezier(Class.BezierCurve bezier)
        {
            this._bezierCurve = bezier;
        }

        public Class.BezierCurve createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            if (this._bezierCurve != null)
            {
                return null;
            }
            else
            {
                this._bezierCurve = new Class.BezierCurve(_nextPoint, borderColor, backgroundColor, borderWidth);
                NextPoint = NextPoint;
                return this._bezierCurve;
            }
            
        }

        private int step = 0;
        public Class.BezierCurve mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            switch (step)
            {
                case 0:
                    if (enable == false)
                        return null;
                    NextPoint = firstPoint;
                    if (_bezierCurve != null)
                    {
                        this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = firstPoint;
                        this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 3] = this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 4];
                    }
                    step++;
                    break;
                case 1:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 1] = firstPoint; step++;
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = firstPoint;
                    break;
                case 2:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = firstPoint;
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 3] = firstPoint;
                    step++; break;
                case 3:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = firstPoint;
                    step = 0; break;
            }
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        bool enable = true;
        public void mouseMove(MouseEventArgs e)
        {
            PointF A = e.Location;
            PointF B = this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2];
            switch (step)
            {
                case 0:
                    double distance = Math.Sqrt(Math.Pow(B.X - A.X, 2) + Math.Pow(B.Y - A.Y, 2));
                    if (enable == true && distance < 5)
                    {
                        return;
                    } else
                        enable = false;

                    NextPoint = e.Location;
                    if (_bezierCurve != null)
                    {
                        this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = e.Location;
                        this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 3] = this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 4];
                    }
                    step++; 
                    break;
                case 1:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 1] = e.Location; 
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = e.Location;
                    break;
                case 2:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = e.Location;
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 3] = e.Location;
                    break;
                case 3:
                    this._bezierCurve.PointList[this._bezierCurve.PointList.Count - 2] = e.Location;
                    enable = true;
                    break;
            }
        }

        public void mouseDoubleClick(Point point)
        {
            step--;
            if ((step == 0) && this._bezierCurve.PointList.Count > 4)
                this._bezierCurve.PointList.RemoveRange(this._bezierCurve.PointList.Count - 3, 3 );
            IsDone = true;
        }
    }
}
