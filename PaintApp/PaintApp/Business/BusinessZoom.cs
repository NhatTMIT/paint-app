﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaintApp.Class;

namespace PaintApp.Business
{
    class BusinessZoom
    {

        public Shape mouseDown(PointF P, MouseButtons e,List<Layer> layerList, Panel panel, ref int BorderWidth, ref int zoomIndex)
        {
            if (e == MouseButtons.Left)
            {
                if (zoomIndex < 8)
                {
                    zoomIndex *= 2;
                    foreach (Layer layer in layerList)
                    {
                        foreach (Handler handler in layer.ShapeList)
                        {
                            ((Shape)handler).BorderWidth = (int)(((Shape)handler).BorderWidth * 2);
                            handler.Zoom(2);
                        }
                    }
                    panel.Size = new Size((int)(panel.Size.Width * 2), (int)(panel.Size.Height * 2));
                    BorderWidth = (int)(BorderWidth * 2);
                }
            }
            else
            {
                if (e == MouseButtons.Right)
                {
                    if (zoomIndex >= 2)
                    {
                        zoomIndex /= 2;
                        foreach (Layer layer in layerList)
                        {
                            foreach (Handler handler in layer.ShapeList)
                            {
                                ((Shape)handler).BorderWidth = (int)(((Shape)handler).BorderWidth / 2);
                                handler.Zoom(0.5f);
                            }
                        }
                        panel.Size = new Size((int)(panel.Size.Width / 2), (int)(panel.Size.Height / 2));
                        BorderWidth = (int)(BorderWidth / 2);
                    }
                }
            }
            return null;
        }

        public static void resieToDefault(List<Layer> layerList, Panel panel, ref int BorderWidth, ref int zoomIndex)
        {
            foreach (Layer layer in layerList)
            {
                foreach (Handler handler in layer.ShapeList)
                {
                    ((Shape)handler).BorderWidth = (int)(((Shape)handler).BorderWidth / zoomIndex);
                    handler.Zoom(1 / (float)zoomIndex);
                }
            }
            panel.Size = new Size((int)(panel.Size.Width / zoomIndex), (int)(panel.Size.Height / zoomIndex));
            BorderWidth = (int)(BorderWidth / zoomIndex);
        }
    }
}
