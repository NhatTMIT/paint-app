﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;

namespace PaintApp.Business
{
    class BusinessEclipse
    {
        private PointF _firstPoint;
        public PointF FirstPoint
        {
            get { return this._firstPoint; }
            set { this._firstPoint = value; }
        }

        private PointF _secondPoint;
        public PointF SecondPoint
        {
            get { return this._secondPoint; }
            set 
            {
                if (_epclipse != null)
                    _epclipse.PointList[1] = value;
                this._secondPoint = value;
            }
        }

        private Class.Ellipse _epclipse;
        public Class.Ellipse EPCLIPSE
        {
            get { return this._epclipse; }
            set { this._epclipse = value; }
        }

        public BusinessEclipse() { }

        public BusinessEclipse(Class.Ellipse eclipse)
        {
            FirstPoint = eclipse.PointList[0];
            SecondPoint = eclipse.PointList[1];
            this._epclipse = eclipse;
        }


        private Class.Ellipse createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            this._epclipse = new Class.Ellipse(FirstPoint, SecondPoint, borderColor, backgroundColor, borderWidth);
            return this._epclipse;
        }

        public Class.Ellipse mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            FirstPoint = firstPoint;
            SecondPoint = firstPoint;
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        public void mouseMove(Point secondPoint)
        {
            SecondPoint = secondPoint;
        }
    }
}
