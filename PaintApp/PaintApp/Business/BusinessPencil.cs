﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;

namespace PaintApp.Business
{
    class BusinessPencil
    {
        private PointF _nextPoint;
        public PointF NextPoint
        {
            get { return this._nextPoint; }
            set 
            {
                if (this._pencil != null)
                {
                    this._pencil.PointList.Add(value);
                    this._pencil.PointList.Add(value);
                    this._pencil.PointList.Add(value);
                }
                this._nextPoint = value; 
            }
        }

        private PointF _currentPoint;
        public PointF CurrentPoint
        {
            get { return this._currentPoint; }
            set { this._currentPoint = value; }
        }

        private Class.Pencil _pencil;
        public Class.Pencil PENCIL
        {
            get { return this._pencil; }
            set { this._pencil = value; }
        }

        public BusinessPencil() 
        {}

        public BusinessPencil(Class.Pencil pencil)
        {
            this._pencil = pencil;
        }

        public Class.Pencil createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            this._pencil = new Class.Pencil(_nextPoint, borderColor, backgroundColor, borderWidth);
            NextPoint = NextPoint;
            return this._pencil;
        }

        public Class.Pencil mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            NextPoint = firstPoint;
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        private int count = 0;
        public void mouseMove(Point secondPoint)
        {  
            if (_pencil.PointList.Count < 4 || (count + 1) % 3 != 0)
            {
                this._pencil.PointList[count] = secondPoint;
            }
            else
            {
                NextPoint = secondPoint;
            }
            count++;
        }
    }
}
