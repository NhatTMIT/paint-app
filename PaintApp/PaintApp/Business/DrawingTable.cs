﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.IO;

namespace PaintApp.Business
{
    public class DrawingTable
    {
        //private Bitmap _backBuffer;
        //public Bitmap BackBuffer
        //{
        //    get { return this._backBuffer; }
        //    set { this._backBuffer = value; }
        //}

        private Bitmap _mainBuffer;
        public Bitmap MainBuffer
        {
            get { return this._mainBuffer; }
            set { this._mainBuffer = value; }
        }

        private Size _sizeImage;
        public Size SizeImage
        {
            get { return this._sizeImage; }
            set
            { 
                this._sizeImage = value;
                changeSizeOfBuffer(value);
            }
        }

        private Color _shapeBackgroundColor = Color.Red;
        public Color ShapeBackgroundColor
        {
            get { return this._shapeBackgroundColor; }
            set { this._shapeBackgroundColor = value; }
        }

        private Color _shapeBorderColor = Color.Black;
        public Color ShapeBorderColor
        {
            get { return this._shapeBorderColor; }
            set { this._shapeBorderColor = value; }
        }

        private int _borderWidth = 3;
        public int BorderWidth
        {
            get { return this._borderWidth; }
            set { this._borderWidth = value; }
        }

        private List<Layer> _layerList;
        public List<Layer> LayerList
        {
            get { return this._layerList; }
            set { this._layerList = value; }
        }

        private int _currentLayer;
        public int CurrentLayer
        {
            get { return this._currentLayer; }
            set { this._currentLayer = value; }
        }

        private Graphics _graphic;
        public Graphics Graphic
        {
            get { return this._graphic; }
            set { this._graphic = value; }
        }

        public enum OptionTool {Select, Zoom, Line, Rectangle, Eclipse, Polygon, BezierCurve, Pen}
        private OptionTool _option = OptionTool.Select;
        public OptionTool Option
        {
            get { return this._option; }
            set
            {
                this._option = value;
                business = null;
                ChangeOptionEvent(value);
            }
        }

        private Shape _selectedShape;
        public Shape SelectedShape
        {
            get { return this._selectedShape; }
            set 
            {
                if (this._selectedShape != null)
                {
                    this._selectedShape.IsSelected = false;
                }

                this._selectedShape = value;

                if (this._selectedShape != null)
                    this._selectedShape.IsSelected = true;
                ShowInfoEvent(this._selectedShape);
            }
        }

        private Panel _panel;
        public Panel PanelShow
        {
            get { return _panel; }
            set { _panel = value; }
        }

        private int _currentZoomIndex = 1;
        public int CurrentZoomIndex
        {
            get { return _currentZoomIndex; }
            set { _currentZoomIndex = value; }
        }

        private string _currentPathFile = "";
        public string CurrentPathFile
        {
            get { return _currentPathFile; }
            set { _currentPathFile = value; }
        }

        public delegate void ShowInfo(Shape shape);
        public event ShowInfo ShowInfoEvent;

        public delegate void ChangeOption(OptionTool option);
        public event ChangeOption ChangeOptionEvent;


        public DrawingTable(Panel panel)
        {
            this.PanelShow = panel;
            this._graphic = panel.CreateGraphics();
            this._graphic.SmoothingMode = SmoothingMode.AntiAlias;
            this._graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            this._graphic.Clear(Color.FromArgb(255, 255, 255));
            SizeImage = panel.Size;
            
            _layerList = new List<Layer>();
            Layer layer = new Layer();
            _layerList.Add(layer);
            CurrentLayer = 0; 
        }

        public void addShape(Shape shape)
        {
            LayerList[CurrentLayer].ShapeList.Add(shape);
        }

        public Layer getCurrentLayer()
        {
            return LayerList[CurrentLayer];
        }

        private void changeSizeOfBuffer(Size size)
        {
            //BackBuffer = new Bitmap(size.Width, size.Height);
            MainBuffer = new Bitmap(size.Width, size.Height);
            this.Graphic = PanelShow.CreateGraphics();
        }

        public void drawLayer()
        {
            Graphics g = null;
            g = Graphics.FromImage(MainBuffer);
            g.Clear(Color.White);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            foreach (Layer i in LayerList)
            {
                i.getGraphic(g);
            }
            
            g.Dispose();
            Graphic.DrawImageUnscaled(MainBuffer, 0, 0);
        }

        //public void drawShape(Shape shape)
        //{
        //    Graphics g = null;
        //    g = Graphics.FromImage(BackBuffer);
        //    g.Clear(Color.White);
        //    g.DrawImageUnscaled(MainBuffer, 0, 0);
            
        //    if (shape != null )
        //        ((Handler)shape).Draw(g);

        //    g.SmoothingMode = SmoothingMode.AntiAlias;
        //    g.Dispose();
        //    Graphic.DrawImageUnscaled(BackBuffer, 0, 0);
        //}

        private Object business;

        public void mouseDown(MouseEventArgs e)
        {
            switch (Option)
            {
                case OptionTool.Select:
                    if (e.Button == MouseButtons.Left)
                    {
                        BusinessSelectTool businessSelectTool = new BusinessSelectTool();
                        this.SelectedShape = businessSelectTool.mouseDown(e.Location, this.LayerList);
                        business = businessSelectTool;
                    }
                    break;
                case OptionTool.Zoom:
                    BusinessZoom businessZoom;
                    if (business == null)
                        businessZoom = new BusinessZoom();
                    else
                        businessZoom = (BusinessZoom)business;
                    this.SelectedShape = businessZoom.mouseDown(e.Location, e.Button, this.LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
                    SizeImage = PanelShow.Size;
                    business = businessZoom;
                    break;
                case OptionTool.Line:
                    if (e.Button == MouseButtons.Left)
                    {
                        BusinessLine businessLine = new BusinessLine();
                        this.SelectedShape = businessLine.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        this.addShape(businessLine.LINE);
                        business = businessLine;
                    }
                    break;

                case OptionTool.Rectangle:
                    if (e.Button == MouseButtons.Left)
                    {
                        BusinessRectangle businessRect = new BusinessRectangle();
                        this.SelectedShape = businessRect.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        this.addShape(businessRect.Rect);
                        business = businessRect;
                    }
                    break;

                case OptionTool.Eclipse:
                    if (e.Button == MouseButtons.Left)
                    {
                        BusinessEclipse businessEclipse = new BusinessEclipse();
                        this.SelectedShape = businessEclipse.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        this.addShape(businessEclipse.EPCLIPSE);
                        business = businessEclipse;
                    }
                    break;

                case OptionTool.Polygon:
                    if (e.Button == MouseButtons.Left)
                    {
                        bool isNew = true;
                        BusinessPolygon businessPolygon;
                        if (business != null)
                        {
                            businessPolygon = (BusinessPolygon)business;
                            if (businessPolygon.IsDone == true)
                                businessPolygon = new BusinessPolygon();
                            else
                                isNew = false;
                        }
                        else
                            businessPolygon = new BusinessPolygon();

                        
                        Shape temp = businessPolygon.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        if (temp != null)
                            this.SelectedShape = temp;
                        if (isNew)
                            this.addShape(businessPolygon.POLYGON);
                        business = businessPolygon;
                    }
                    break;

                case OptionTool.Pen:
                    if (e.Button == MouseButtons.Left)
                    {
                        BusinessPencil businessPencil = new BusinessPencil();
                        this.SelectedShape = businessPencil.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        this.addShape(businessPencil.PENCIL);
                        business = businessPencil;
                    }
                    break;

                case OptionTool.BezierCurve:
                    if (e.Button == MouseButtons.Left)
                    {
                        bool isNew = true;
                        BusinessBezier businessBezier;
                        if (business != null)
                        {
                            businessBezier = (BusinessBezier)business;
                            if (businessBezier.IsDone == true)
                                businessBezier = new BusinessBezier();
                            else
                                isNew = false;

                        }
                        else
                            businessBezier = new BusinessBezier();

                        Shape tempBezier = businessBezier.mouseDown(e.Location, ShapeBackgroundColor, ShapeBorderColor, BorderWidth);
                        if (tempBezier != null)
                            this.SelectedShape = tempBezier;
                        if (isNew)
                            this.addShape(businessBezier.BEZIER);
                        business = businessBezier;
                    }
                    break;
            }
            drawLayer();
            ShowInfoEvent(this._selectedShape);
            //drawShape(this.SelectedShape);
        }

        public void mouseMove(MouseEventArgs e)
        {
            switch (Option)
            {
                case OptionTool.Select:
                    if (e.Button == MouseButtons.Left)
                    {
                        ((BusinessSelectTool)business).mouseMove(e.Location, this.SelectedShape);
                    }
                    break;
                case OptionTool.Line:
                    if (e.Button == MouseButtons.Left)
                    {
                        ((BusinessLine)business).mouseMove(e.Location);
                    }
                    break;
                case OptionTool.Rectangle:
                    if (e.Button == MouseButtons.Left)
                    {
                        ((BusinessRectangle)business).mouseMove(e.Location);
                    }
                    break;
                case OptionTool.Eclipse:
                    if (e.Button == MouseButtons.Left)
                    {
                        ((BusinessEclipse)business).mouseMove(e.Location);
                    }
                    break;
                case OptionTool.Polygon:
                    if (business != null && ((BusinessPolygon)business).IsDone == false)
                        ((BusinessPolygon)business).mouseMove(e.Location);
                    break;

                case OptionTool.Pen:
                    if (e.Button == MouseButtons.Left)
                    {
                        ((BusinessPencil)business).mouseMove(e.Location);
                    }
                    break;

                case OptionTool.BezierCurve:
                    if (business != null && ((BusinessBezier)business).IsDone == false)
                        ((BusinessBezier)business).mouseMove(e);
                    break;
            }
            drawLayer();
            ShowInfoEvent(this._selectedShape);
            //drawShape(this.SelectedShape);
        }

        public void mouseUp(MouseEventArgs e)
        {
            switch (Option)
            {
                case OptionTool.Select: break;
                case OptionTool.Pen:
                    
                    break;
                default: 
                    //MainBuffer = (Bitmap)BackBuffer.Clone();
                    //drawLayer();
                    break;
            }
        }

        public void mouseDoubleClick(MouseEventArgs e)
        {
            switch (Option)
            {
                case OptionTool.Select: break;
                case OptionTool.Polygon:
                    ((BusinessPolygon)business).mouseDoubleClick(e.Location);
                    break;
                case OptionTool.BezierCurve:
                    ((BusinessBezier)business).mouseDoubleClick(e.Location);
                    break;
                default:
                    //MainBuffer = (Bitmap)BackBuffer.Clone();
                    //drawLayer();
                    break;
            }
        }

        public void saveFile()
        {
            BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
            this.SelectedShape = null;
            drawLayer();
            BusinessStoreAndRead.storeFile(this.LayerList, MainBuffer , ref _currentPathFile);
        }

        public void saveAsFile()
        {
            string temp = "";
            BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
            this.SelectedShape = null;
            drawLayer();
            BusinessStoreAndRead.storeFile(this.LayerList, MainBuffer, ref temp);
        }

        public void readFile()
        {
            int option = BusinessStoreAndRead.cautionSaveFile();
            if (option == 0)
            {
                BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
                this.SelectedShape = null;
                drawLayer();
                BusinessStoreAndRead.storeFile(this.LayerList, MainBuffer , ref _currentPathFile);
            }
            else
            {
                if (option == 2)
                    return;
            }

            BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
            List<Layer> layerList;
            layerList = BusinessStoreAndRead.readFile(ref _currentPathFile);
            if (layerList != null)
            {
                this.LayerList = layerList;
                drawLayer();
            }
        }

        public bool newFile()
        {
            int option = BusinessStoreAndRead.cautionSaveFile();
            if (option == 0)
            {
                BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
                this.SelectedShape = null;
                drawLayer();
                BusinessStoreAndRead.storeFile(this.LayerList, MainBuffer, ref _currentPathFile);
            }
            else
            {
                if (option == 2)
                    return false;
            }
            BusinessZoom.resieToDefault(LayerList, PanelShow, ref _borderWidth, ref _currentZoomIndex);
            return true;
        }

        

    }
}
