﻿using PaintApp.Class;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Business
{
    class BusinessLine
    {
        private PointF _firstPoint;
        public PointF FirstPoint
        {
            get { return this._firstPoint; }
            set { this._firstPoint = value; }
        }

        private PointF _secondPoint;
        public PointF SecondPoint
        {
            get { return this._secondPoint; }
            set 
            {
                if (_line != null)
                    _line.PointList[1] = value;
                this._secondPoint = value;
            }
        }

        private Line _line;
        public Line LINE
        {
            get { return this._line; }
            set { this._line = value; }
        }

        public BusinessLine() { }

        public BusinessLine(Line line)
        {
            FirstPoint = line.PointList[0];
            SecondPoint = line.PointList[1];
            this._line = line;
        }

        public Line createShape(Color backgroundColor, Color borderColor, int borderWidth)
        {
            this._line = new Line(FirstPoint, SecondPoint, borderColor, backgroundColor, borderWidth);
            return this._line;
        }

        public Line mouseDown(Point firstPoint, Color shapeBackgroundColor, Color shapeBorderColor, int borderWidth)
        {
            FirstPoint = firstPoint;
            SecondPoint = firstPoint;
            return createShape(shapeBackgroundColor, shapeBorderColor, borderWidth);
        }

        public void mouseMove(Point secondPoint)
        {
            SecondPoint = secondPoint;
        }



    }
}
