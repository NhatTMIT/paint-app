﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaintApp.Class;
using System.Drawing;
using System.Windows.Forms;

namespace PaintApp.Business
{
    class BusinessSelectTool
    {
        PointF FirstPoint;
        List<PointF> firstPointList;

        public Shape mouseDown(PointF selectPoint, List<Layer> layerList)
        {
            FirstPoint = selectPoint;
            for (int i = layerList.Count - 1; i >= 0 ; i--)
            {
                for (int j = layerList[i].ShapeList.Count - 1; j >= 0; j--)
                {
                    PointF tempPoint = Rotation.RotatePoint(selectPoint, layerList[i].ShapeList[j].CenterPoint, -layerList[i].ShapeList[j].CornerRotate);
                    if (((Handler)layerList[i].ShapeList[j]).checkSelected(tempPoint))
                    {
                        firstPointList = new List<PointF>();
                        firstPointList.AddRange(layerList[i].ShapeList[j].PointList);
                        return layerList[i].ShapeList[j];
                    }
                }
            }
            return null;
        }


        public void mouseMove(PointF point, Shape currentShape)
        {
            if (currentShape != null)
            {
                Cursor.Current = Cursors.SizeAll;
                float[] vector = new float[2];

                List<PointF> tempList = new List<PointF>();
                tempList.AddRange(firstPointList);

                currentShape.PointList = tempList;
                
                vector[0] = point.X - FirstPoint.X;
                vector[1] = point.Y - FirstPoint.Y;
                ((Handler)currentShape).Translate(vector);
            }
        }


    }
}
