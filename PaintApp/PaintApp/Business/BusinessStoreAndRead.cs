﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using PaintApp.Class;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace PaintApp.Business
{
    class BusinessStoreAndRead
    {
        public static void storeFile(List<Layer> layerList, Bitmap image, ref string path)
        {
            if (path == "")
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                saveFileDialog.Filter = @"Paint App files |*.paintapp|
PNG Image|*.png|
JPEG Image |*.jpeg|
ICO Image |*.ico|
Gif Image |*.gif|
Bitmap Image|*.bmp";

                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    path = saveFileDialog.FileName;
                else
                    return;

                if (saveFileDialog.FilterIndex == 2)
                {
                    image.Save(path, ImageFormat.Png);
                    path = "";
                    return;
                }

                if (saveFileDialog.FilterIndex == 3)
                {
                    image.Save(path, ImageFormat.Jpeg);
                    path = "";
                    return;
                }

                if (saveFileDialog.FilterIndex == 4)
                {
                    image.Save(path, ImageFormat.Icon);
                    path = "";
                    return;
                }

                if (saveFileDialog.FilterIndex == 5)
                {
                    image.Save(path, ImageFormat.Gif);
                    path = "";
                    return;
                }

                if (saveFileDialog.FilterIndex == 6)
                {
                    image.Save(path, ImageFormat.Bmp);
                    path = "";
                    return;
                }

            }
            
            using (StreamWriter sw = new StreamWriter(Path.GetFullPath(path)))
            {
                sw.WriteLine(JsonConvert.SerializeObject(layerList));
            }
        }

        public static int cautionSaveFile()
        {
            DialogResult result = MessageBox.Show("Do you want to save?", "Warning",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                return 0;
            }
            else if (result == DialogResult.No)
            {
                return 1;
            }

            return 2;
        }

        public static List<Layer> readFile(ref string path)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "paintapp files (*.paintapp)|*.paintapp";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string line = "";
                    string strJson = "";
                    path = openFileDialog.FileName;
                    using (StreamReader sr = new StreamReader(Path.GetFullPath(path)))
                    {
                        while ((line = sr.ReadLine()) != null)
                        {
                            strJson += line;
                        }
                    }
                    List<Layer> layerList = JsonConvert.DeserializeObject<List<Layer>>(strJson);

                    foreach (Layer layer in layerList)
                    {
                        for (int i = 0; i < layer.ShapeList.Count; i++)
                        {
                            switch (layer.ShapeList[i].TYPE)
                            {
                                case Shape.TYPEMEMBER.BEZIERCURVE:
                                    layer.ShapeList[i] = new Class.BezierCurve(layer.ShapeList[i]);
                                    break;

                                case Shape.TYPEMEMBER.ELLIPSE:
                                    layer.ShapeList[i] = new Class.Ellipse(layer.ShapeList[i]);
                                    break;

                                case Shape.TYPEMEMBER.LINE:
                                    layer.ShapeList[i] = new Class.Line(layer.ShapeList[i]);
                                    break;

                                case Shape.TYPEMEMBER.PENCIL:
                                    layer.ShapeList[i] = new Class.Pencil(layer.ShapeList[i]);
                                    break;

                                case Shape.TYPEMEMBER.POLYGON:
                                    layer.ShapeList[i] = new Class.Polygon(layer.ShapeList[i]);
                                    break;

                                case Shape.TYPEMEMBER.RECTANGLE:
                                    layer.ShapeList[i] = new Class.Rectangle(layer.ShapeList[i]);
                                    break;
                            }
                        }
                    }
                    return layerList;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return null;
        }
    }
}
