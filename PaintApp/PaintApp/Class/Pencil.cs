﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp.Class
{
    public class Pencil : Shape, Handler
    {
        public Pencil()
        {
            TYPE = TYPEMEMBER.PENCIL;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
        }

        public Pencil(PointF point1, Color borderColor, Color layerColor, int borderwidth)
        {
            TYPE = TYPEMEMBER.PENCIL;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();

            PointList.Add(point1);
            GroundPane.FindLimit(PointList);

            BorderWidth = borderwidth;
            BorderColor = borderColor;
            BackGroundColor = layerColor;
        }

        public Pencil(Shape shape)
        {
            TYPE = TYPEMEMBER.PENCIL;
            this.CornerRotate = shape.CornerRotate;
            this.PointList = shape.PointList;
            this.BorderWidth = shape.BorderWidth;
            this.BorderColor = shape.BorderColor;
            this.BackGroundColor = shape.BackGroundColor;
            this.CenterPoint = shape.CenterPoint;
            this.GroundPane = shape.GroundPane;
        }

        public GraphicsPath DrawPath(List<PointF> points)
        {
            GraphicsPath grap = new GraphicsPath();
            //PointF[] temp = new PointF[PointList.Count - 2];
            //PointList.CopyTo(1, temp, 0, PointList.Count - 2);
            grap.AddBeziers(PointList.ToArray());
            return grap;
        }

        public void SetCenterPoint()
        {
            GroundPane.FindLimit(PointList);
            CenterPoint = new PointF((GroundPane.limitPoint[0].X + GroundPane.limitPoint[4].X) / 2,
                (GroundPane.limitPoint[0].Y + GroundPane.limitPoint[4].Y) / 2);
        }

        public Graphics Draw(Graphics g)
        {
            SolidBrush BackGroundSB = new SolidBrush(BackGroundColor);
            Pen BorderPen = new Pen(BorderColor, BorderWidth);

            Matrix myMatrix = new Matrix();
            SetCenterPoint();
            myMatrix.RotateAt(CornerRotate, CenterPoint);

            //Rotate(CornerRotate);
            GroundPane.FindLimit(PointList);
            g.Transform = myMatrix;

            g.DrawPath(BorderPen, DrawPath(PointList));
            if (IsSelected == true)
            {
                float[] dashValues = { 5, 5 };
                Pen pen = new Pen(Color.Black, 1);
                pen.DashPattern = dashValues;
                g.DrawPath(pen, GroundPane.DrawPane());
                GroundPane.DrawLimitPoint(g);
                DrawPoint(g);
            }
            return g;
        }

        public void Zoom(float zoomIndex)
        {
            for (int i = 0; i < PointList.Count(); i++)
            {
                PointList[i] = new PointF((PointList[i].X) * zoomIndex,
                    (PointList[i].Y) * zoomIndex);
            }
        }

        public void Translate(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X + target[0], PointList[i].Y + target[1]);
            SetCenterPoint();
        }

        public void Shear(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X * target[0], PointList[i].Y * target[1]);
            SetCenterPoint();
        }

        public void DrawPoint(Graphics e)
        {
            foreach (PointF p in PointList)
                e.FillEllipse(new SolidBrush(Color.Blue), p.X - 5, p.Y - 5, 10, 10);
        }

        public bool checkSelected(PointF p)
        {
            return DrawPath(PointList).IsVisible(p);
        }
    }
}
