﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Class
{
    public class Rectangle : Shape, Handler
    {
        public Rectangle()// khởi tạo giá trị
        {
            TYPE = TYPEMEMBER.RECTANGLE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
            SetCenterPoint();
        }

        public Rectangle(PointF point1, PointF point2, Color borderColor, Color layerColor, int borderwidth)// khởi tạo với điểm đầu, màu viền, màu nền, kích thước viền
        {
            TYPE = TYPEMEMBER.RECTANGLE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();

            PointList.Add(point1);
            PointList.Add(point2);
            GroundPane.FindLimit(PointList);

            BorderWidth = borderwidth;
            BorderColor = borderColor;
            BackGroundColor = layerColor;
            SetCenterPoint();
        }

        public Rectangle(Shape shape)// gán giá trị của class cha Shape lại cho Rectangle
        {
            TYPE = TYPEMEMBER.RECTANGLE;
            this.CornerRotate = shape.CornerRotate;
            this.PointList = shape.PointList;
            this.BorderWidth = shape.BorderWidth;
            this.BorderColor = shape.BorderColor;
            this.BackGroundColor = shape.BackGroundColor;
            this.CenterPoint = shape.CenterPoint;
            this.GroundPane = shape.GroundPane;
            SetCenterPoint();
        }

        public void SetCenterPoint()// tìm tâm hình 
        {
            CenterPoint = new PointF((PointList[0].X + PointList[1].X) / 2, (PointList[0].Y + PointList[1].Y) / 2);
        }

        public GraphicsPath DrawPath(List<PointF> points)// xác định đường viền của hình chữ nhật
        {
            GraphicsPath path = new GraphicsPath();
            RectangleF rect = new RectangleF();
            // Từ 2 Point giới hạn trái,phại,trên,dưới tính toán và xác định kích cỡ của hình rectangle và điểm khởi tạo
            // do hàm AddRectangleF không overload vị trí 2 điểm Points
            if (PointList[0].X < PointList[1].X && PointList[0].Y < PointList[1].Y)
            {
                rect = new RectangleF(PointList[0].X, PointList[0].Y,
                PointList[1].X - PointList[0].X, PointList[1].Y - PointList[0].Y);
            }
            if (PointList[0].X < PointList[1].X && PointList[0].Y >= PointList[1].Y)
            {
                rect = new RectangleF(PointList[0].X, PointList[1].Y,
                PointList[1].X - PointList[0].X, PointList[0].Y - PointList[1].Y);
            }
            if (PointList[0].X >= PointList[1].X && PointList[0].Y < PointList[1].Y)
            {
                rect = new RectangleF(PointList[1].X, PointList[0].Y,
                PointList[0].X - PointList[1].X, PointList[1].Y - PointList[0].Y);
            }
            if (PointList[0].X >= PointList[1].X && PointList[0].Y >= PointList[1].Y)
            {
                rect = new RectangleF(PointList[1].X, PointList[1].Y,
                PointList[0].X - PointList[1].X, PointList[0].Y - PointList[1].Y);
            }

            path.AddRectangle(rect);
            return path;
        }

        public Graphics Draw(Graphics g)// vẽ hình lên graphics
        {
            // khởi tạo SolidBrush, Pen để tô màu và vẽ đường viền cho hình
            SolidBrush BackGroundSB = new SolidBrush(BackGroundColor);
            Pen BorderPen = new Pen(BorderColor, BorderWidth);

            Matrix myMatrix = new Matrix();// khởi tạo không gian ma trận
            SetCenterPoint();// tìm tâm GroundPane
            myMatrix.RotateAt(CornerRotate, CenterPoint);// xoay hình theo matrix với tâm là CenterPoint

            //Rotate(CornerRotate);
            GroundPane.FindLimit(PointList);// xác định lại 8 điểm giới hạn GroundPane
            g.Transform = myMatrix;// gán Graphics cho ma trận xoay khi được xoay 

            g.FillPath(BackGroundSB, DrawPath(PointList));// tô màu nền cho rectangle
            g.DrawPath(BorderPen, DrawPath(PointList));// vẽ đường viền cho hình
            if (IsSelected == true)// nếu click chuột trong không gian giới hạn của GroundPane
            {
                float[] dashValues = { 5, 5 };
                Pen pen = new Pen(Color.Black, 1);
                pen.DashPattern = dashValues;
                g.DrawPath(pen, GroundPane.DrawPane());// vẽ đường viền của GroundPane
                GroundPane.DrawLimitPoint(g);// Vẽ các điểm giới hạn GroundPane
                DrawPoint(g);// vẽ các đỉnh của rectangle
            }
            return g;
        }

        public void Zoom(float zoomIndex)// phóng hình theo hệ số zoom
        {
            for (int i = 0; i < PointList.Count(); i++)// nhân các đỉnh của rectangle cho hệ số nhân
            {
                PointList[i] = new PointF((PointList[i].X) * zoomIndex,
                    (PointList[i].Y) * zoomIndex);
            }
        }

        public void Translate(float[] target)// tịnh tiến hình 
        {
            for (int i = 0; i < PointList.Count(); i++)// Công các điểm cho vector tịnh tiến
                PointList[i] = new PointF(PointList[i].X + target[0], PointList[i].Y + target[1]);
            SetCenterPoint();// cập nhật lại centerPoint
        }

        public void Shear(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X * target[0], PointList[i].Y * target[1]);
            SetCenterPoint();
        }

        public void DrawPoint(Graphics e)// vẽ 4 góc của rectangle được xác định theo hình chữ nhật
        {
            e.FillEllipse(new SolidBrush(Color.Blue), PointList[0].X - 5, PointList[0].Y - 5, 10, 10);
            e.FillEllipse(new SolidBrush(Color.Blue), PointList[1].X - 5, PointList[1].Y - 5, 10, 10);
            e.FillEllipse(new SolidBrush(Color.Blue), PointList[1].X - 5, PointList[0].Y - 5, 10, 10);
            e.FillEllipse(new SolidBrush(Color.Blue), PointList[0].X - 5, PointList[1].Y - 5, 10, 10);
        }

        public bool checkSelected(PointF p)
        {
            return DrawPath(PointList).IsVisible(p);
        }
    }
}
