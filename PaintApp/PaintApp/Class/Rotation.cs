﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp.Class
{
    public class Rotation
    {
        public static PointF RotatePoint(PointF pointToRotate, PointF centerPoint, float angleInDegrees)// tính ra vị trí của 1 điểm  khi xoay quanh tâm tròn với 1 góc quay,
        {
            double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            float X = (float)(cosTheta * (pointToRotate.X - centerPoint.X) - sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X);

            float Y = (float)(sinTheta * (pointToRotate.X - centerPoint.X) + cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y);

            return new PointF(X, Y);
        }
    }
}
