﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PaintApp.Class
{
    public class Shape
    {
        public enum TYPEMEMBER { LINE, ELLIPSE, POLYGON, RECTANGLE, BEZIERCURVE, PENCIL }// kiểu hình

        protected TYPEMEMBER _type;
        public TYPEMEMBER TYPE// biến lưu kiểu hình của các lớp con của Shape
        {
            get { return _type; }
            set { _type = value; }
        }

        private PointF _centerPoint;
        public PointF CenterPoint// điểm trung tâm của hình
        {
            get { return _centerPoint; }
            set { _centerPoint = value; }
        }

        private float _cornerRotate;
        public float CornerRotate// góc xoay
        {
            get
            {
                return _cornerRotate;
            }

            set
            {
                _cornerRotate = value;
            }
        }

        private int _borderWidth;
        public int BorderWidth// kích thước đường viền
        {
            get { return _borderWidth; }
            set { _borderWidth = value; }
        }

        private List<PointF> _pointList;
        public List<PointF> PointList// danh sách các điểm xác định hình
        {
            get
            {
                return _pointList;
            }
            set
            {
                _pointList = value;
            }
        }

        //private List<PointF> _pointSave;
        //public List<PointF> PointSave
        //{
        //    get { return _pointSave; }
        //    set { _pointSave = value; }
        //}

        private Pane _groundPane;
        public Pane GroundPane// Pane tấm nên nằm dưới hình để xác định vị trị và không gian hình chiếm giữ
        {
            get { return _groundPane; }
            set { _groundPane = value; }
        }

        private Color _borderColor;
        public Color BorderColor// màu đường biên
        {
            get
            {
                return _borderColor;
            }
            set
            {
                _borderColor = value;
            }
        }

        private Color _backGroundColor;
        public Color BackGroundColor// màu nền
        {
            get { return _backGroundColor; }
            set { _backGroundColor = value; }
        }

        private bool _isSelected;
        public bool IsSelected// biến xét hình được chọn hay không
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

    }
}
