﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Class
{
    class Line : Shape, Handler
    {
        public Line()// khởi tạo giá trị cho Line
        {
            TYPE = TYPEMEMBER.LINE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
        }

        public Line(PointF point1, PointF point2, Color borderColor, Color layerColor, int borderwidth)// khởi tạo với điểm đầu, màu viền, màu nền, kích thước viền
        {
            TYPE = TYPEMEMBER.LINE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();

            PointList.Add(point1);
            PointList.Add(point2);
            GroundPane.FindLimit(PointList);

            BorderWidth = borderwidth;
            BorderColor = borderColor;
            BackGroundColor = layerColor;
        }

        public Line(Shape shape)// gán giá trị của class cha Shape lại cho Line
        {
            TYPE = TYPEMEMBER.LINE;
            this.CornerRotate = shape.CornerRotate;
            this.PointList = shape.PointList;
            this.BorderWidth = shape.BorderWidth;
            this.BorderColor = shape.BorderColor;
            this.BackGroundColor = shape.BackGroundColor;
            this.CenterPoint = shape.CenterPoint;
            this.GroundPane = shape.GroundPane;
        }

        public GraphicsPath DrawPath(List<PointF> points)// tạo GraphicsPath với 2 Points cho hình Line
        {
            GraphicsPath path = new GraphicsPath();// tạo GraphicsPath
            path.AddLine(points[0], points[1]);// add 2 điểm Points cho path
            return path;
        }

        public void SetCenterPoint()// xác định trung điểm đường thẳng
        {
            CenterPoint = new PointF((PointList[0].X + PointList[1].X) / 2, (PointList[0].Y + PointList[1].Y) / 2);
        }

        public void Rotate(float m_cornerRotate)// xoay đường thẳng
        {
            CornerRotate = m_cornerRotate;
            for (int i = 0; i < PointList.Count(); i++)// dùng hàm RotatePoint để xác định điểm mới của Points sau khi xoay với tâm là CenterPoint
                PointList[i] = Rotation.RotatePoint(PointList[i], CenterPoint, CornerRotate);
        }

        public Graphics Draw(Graphics g)// vẽ đường thẳng ra Graphics
        {
            // khởi tạo SolidBrush, Pen để tô màu và vẽ đường viền cho hình
            SolidBrush BackGroundSB = new SolidBrush(BackGroundColor);
            Pen BorderPen = new Pen(BorderColor, BorderWidth);

            Matrix myMatrix = new Matrix(); // tạo không gian ma trận
            SetCenterPoint();// tìm tâm GroundPane
            myMatrix.RotateAt(CornerRotate, CenterPoint);// xoay hình theo matrix với tâm là CenterPoint

            //Rotate(CornerRotate);
            GroundPane.FindLimit(PointList);// xác định lại 8 điểm giới hạn GroundPane
            g.Transform = myMatrix;// gán Graphics cho ma trận xoay khi được xoay 

            g.FillPath(BackGroundSB, DrawPath(PointList));// tô màu nền cho ellipse
            g.DrawPath(BorderPen, DrawPath(PointList));// vẽ đường viền cho hình
            if (IsSelected == true)// nếu click chuột trong không gian giới hạn của đường thẳng
            {
                DrawPoint(g);// vẽ 2 điểm của đường thẳng
            }
            return g;
        }

        public void Zoom(float zoomIndex)// phóng hình theo hệ số zoom
        {
            for (int i = 0; i < PointList.Count(); i++)// nhân các đỉnh của Ellipse cho hệ số nhân
            {
                PointList[i] = new PointF((PointList[i].X) * zoomIndex,
                    (PointList[i].Y) * zoomIndex);
            }
        }

        public void Translate(float[] target)// tịnh tiến hình 
        {
            for (int i = 0; i < PointList.Count(); i++)// Công các điểm cho vector tịnh tiến
                PointList[i] = new PointF(PointList[i].X + target[0], PointList[i].Y + target[1]);
        }

        public void Shear(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X * target[0], PointList[i].Y * target[1]);
            SetCenterPoint();
        }

        public void DrawPoint(Graphics e)// vẽ 2 điểm của đường thẳng ra Graphics
        {
            foreach (PointF p in PointList)
                e.FillEllipse(new SolidBrush(Color.Blue), p.X - 5, p.Y - 5, 10, 10);
        }

        public bool checkSelected(PointF P)
        {
            PointF A = PointList[0];
            PointF B = PointList[1];

            //Heron Formular
            double a = Math.Sqrt(Math.Pow(B.X - A.X, 2) + Math.Pow(B.Y - A.Y, 2));
            double b = Math.Sqrt(Math.Pow(B.X - P.X, 2) + Math.Pow(B.Y - P.Y, 2));
            double c = Math.Sqrt(Math.Pow(P.X - A.X, 2) + Math.Pow(P.Y - A.Y, 2));
            double p = (a + b + c) / (float)2;


            GroundPane.CheckInside(P);
            if (!GroundPane.IsSelected)
                return false;
            
            double distance = 2 * Math.Sqrt(p * (p - a) * (p - b) * (p - c)) / a;
            
            return distance <= (5 + BorderWidth);
        }
        
    }
}
