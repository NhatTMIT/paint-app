﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PaintApp.Class
{
    public class Layer
    {
        private List<Shape> shapeList;

        private Graphics graphicCache;
        public Graphics GraphicCache
        {
            get { return this.graphicCache; }
            set { this.graphicCache = value; }
        }

        public List<Shape> ShapeList
        {
            get { return this.shapeList; }
            set { this.shapeList = value; }
        }

        public Layer()
        {
            shapeList = new List<Shape>();
        }

        public Graphics getGraphic(Graphics g)
        {
            foreach (Handler i in shapeList)
            {
                i.Draw(g);
            }

            return g;
        }
    }
}
