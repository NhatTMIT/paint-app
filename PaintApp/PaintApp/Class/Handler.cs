﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp.Class
{
    interface Handler// interface chứa các phương thức thao tác với 1 hình
    {
        Graphics Draw(Graphics g);// vẽ hình ra Graphics

        GraphicsPath DrawPath(List<PointF> points);// tạo đường biên của hình

        void SetCenterPoint();// xác định điểm trung tâm hình

        void Zoom(float zoomIndex);// phóng to thu nhỏ

        void Translate(float[] target);// tịnh tiến hình

        void Shear(float[] target);

        void DrawPoint(Graphics e);// vẽ các đỉnh của hình

        bool checkSelected(PointF p);
    }
}
