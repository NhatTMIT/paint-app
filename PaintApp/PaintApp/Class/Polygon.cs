﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Class
{
    public class Polygon : Shape, Handler
    {
        

        public Polygon()// khởi tạo giá trị
        {
            TYPE = TYPEMEMBER.POLYGON;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
        }

        public Polygon(PointF point1, Color borderColor, Color layerColor, int borderwidth)// khởi tạo với điểm đầu, màu viền, màu nền, kích thước viền
        {
            TYPE = TYPEMEMBER.POLYGON;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();

            PointList.Add(point1);
            GroundPane.FindLimit(PointList);

            BorderWidth = borderwidth;
            BorderColor = borderColor;
            BackGroundColor = layerColor;
        }

        public Polygon(Shape shape)// gán giá trị của class cha Shape lại cho Polygon
        {
            TYPE = TYPEMEMBER.POLYGON;
            this.CornerRotate = shape.CornerRotate;
            this.PointList = shape.PointList;
            this.BorderWidth = shape.BorderWidth;
            this.BorderColor = shape.BorderColor;
            this.BackGroundColor = shape.BackGroundColor;
            this.CenterPoint = shape.CenterPoint;
            this.GroundPane = shape.GroundPane;
        }

        public GraphicsPath DrawPath(List<PointF> points)// xác định đường viền của hình Polygon
        {
            GraphicsPath grap = new GraphicsPath();
            if (PointList.Count > 2)// nếu tồn tại nhiều hơn 2 điểm vẽ Polygon
                grap.AddPolygon(PointList.ToArray());
            else// ngược lại vẽ đường thẳng
                grap.AddLine(PointList[0], PointList[1]);
            return grap;
        }

        public Graphics Draw(Graphics g)// vẽ hình lên graphics
        {
            // khởi tạo SolidBrush, Pen để tô màu và vẽ đường viền cho hình
            SolidBrush BackGroundSB = new SolidBrush(BackGroundColor);
            Pen BorderPen = new Pen(BorderColor, BorderWidth);

            Matrix myMatrix = new Matrix();// khởi tạo không gian ma trận
            SetCenterPoint();// tìm tâm GroundPane
            myMatrix.RotateAt(CornerRotate, CenterPoint);// xoay hình theo matrix với tâm là CenterPoint

            //Rotate(CornerRotate);
            GroundPane.FindLimit(PointList);// xác định lại 8 điểm giới hạn GroundPane
            g.Transform = myMatrix;// gán Graphics cho ma trận xoay khi được xoay 

            g.FillPath(BackGroundSB, DrawPath(PointList));// tô màu nền cho rectangle
            g.DrawPath(BorderPen, DrawPath(PointList));// vẽ đường viền cho hình
            if (IsSelected == true)// nếu click chuột trong không gian giới hạn của GroundPane
            {
                float[] dashValues = { 5, 5 };
                Pen pen = new Pen(Color.Black, 1);
                pen.DashPattern = dashValues;
                g.DrawPath(pen, GroundPane.DrawPane());// vẽ đường viền của GroundPane
                GroundPane.DrawLimitPoint(g);// Vẽ các điểm giới hạn GroundPane
                DrawPoint(g);// vẽ các đỉnh của Polygon
            }
            return g;
        }

        public void SetCenterPoint()// xác định điểm trung tâm hình
        {
            CenterPoint = new PointF((GroundPane.limitPoint[0].X + GroundPane.limitPoint[4].X) / 2,
                (GroundPane.limitPoint[0].Y + GroundPane.limitPoint[4].Y) / 2);
        }

        public void Zoom(float zoomIndex)// phóng hình theo hệ số zoom
        {
            for (int i = 0; i < PointList.Count(); i++)// nhân các đỉnh của rectangle cho hệ số nhân
            {
                PointList[i] = new PointF((PointList[i].X) * zoomIndex,
                    (PointList[i].Y) * zoomIndex);
            }
        }

        public void Translate(float[] target)// tịnh tiến hình 
        {
            for (int i = 0; i < PointList.Count(); i++)// Công các điểm cho vector tịnh tiến
                PointList[i] = new PointF(PointList[i].X + target[0], PointList[i].Y + target[1]);
            SetCenterPoint();// cập nhật lại centerPoint
        }

        public void Shear(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X * target[0], PointList[i].Y * target[1]);
            SetCenterPoint();
        }

        public void DrawPoint(Graphics e)// vẽ các đỉnh của hình Polygon
        {
            foreach (PointF p in PointList)
                e.FillEllipse(new SolidBrush(Color.Blue), p.X - 5, p.Y - 5, 10, 10);
        }

        public bool checkSelected(PointF p)
        {
            return DrawPath(PointList).IsVisible(p);
        }

    }
}
