﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp.Class
{
    public class Pane
    {
        private bool _isSelected;
        public bool IsSelected// biến điều khiển cho biến Pane có được chọn hay không
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private PointF[] _limitPoint;
        public PointF[] limitPoint// 8 điểm giới hạn của Pane
        {
            get { return _limitPoint; }
            set { _limitPoint = value; }
        }

        private int _orderLayer;
        public int orderLayer// số thứ tự Pane
        {
            get { return _orderLayer; }
            set { _orderLayer = value; }
        }

        public Pane()// khởi tạo giá trị
        {
            limitPoint = new PointF[8];
        }

        public void FindLimit(List<PointF> points)// tìm 8 điểm giới hạn
        {
            limitPoint[0].X = limitPoint[4].X = points[0].X;// gán giá trị ban đầu
            limitPoint[0].Y = limitPoint[4].Y = points[0].Y;
            foreach (PointF p in points)// tìm ra 4 điểm góc trái, phải, trên, dưới
            {
                if (p.X < limitPoint[0].X)
                    limitPoint[0].X = p.X;
                if (p.X > limitPoint[4].X)
                    limitPoint[4].X = p.X;
                if (p.Y < limitPoint[0].Y)
                    limitPoint[0].Y = p.Y;
                if (p.Y > limitPoint[4].Y)
                    limitPoint[4].Y = p.Y;
            }
            // các điểm còn lại lần lượt là trung điểm của 4 đường thẳng
            limitPoint[1] = new PointF((limitPoint[0].X + limitPoint[4].X) / 2, limitPoint[0].Y);
            limitPoint[2] = new PointF(limitPoint[4].X, limitPoint[0].Y);
            limitPoint[3] = new PointF(limitPoint[4].X, (limitPoint[0].Y + limitPoint[4].Y) / 2);
            limitPoint[5] = new PointF(limitPoint[1].X, limitPoint[4].Y);
            limitPoint[6] = new PointF(limitPoint[0].X, limitPoint[4].Y);
            limitPoint[7] = new PointF(limitPoint[0].X, limitPoint[3].Y);

        }

        public void CheckInside(PointF point)// hàm kiểm tra xem Pane được chọn hay không
        {
            // nếu click chuột nằm trong đường viền giới hạn hình chữ nhật thì IsSelected = true
            if (point.X >= limitPoint[0].X && point.X <= limitPoint[4].X && point.Y >= limitPoint[0].Y && point.Y <= limitPoint[4].Y)
                IsSelected = true;
            else
                IsSelected = false;
        }

        public GraphicsPath DrawPane()// xác định đường biên cho Pane
        {
            GraphicsPath gra = new GraphicsPath();
            RectangleF rect = new RectangleF();
            // Từ 2 Point giới hạn trái,phại,trên,dưới tính toán và xác định kích cỡ của hình rectangle và điểm khởi tạo
            // do hàm AddRectangleF không overload vị trí 2 điểm Points
            if (limitPoint[0].X < limitPoint[4].X && limitPoint[0].Y < limitPoint[4].Y)
            {
                rect = new RectangleF(limitPoint[0].X, limitPoint[0].Y,
                limitPoint[4].X - limitPoint[0].X, limitPoint[4].Y - limitPoint[0].Y);
            }
            if (limitPoint[0].X < limitPoint[4].X && limitPoint[0].Y >= limitPoint[4].Y)
            {
                rect = new RectangleF(limitPoint[0].X, limitPoint[4].Y,
                limitPoint[4].X - limitPoint[0].X, limitPoint[0].Y - limitPoint[4].Y);
            }
            if (limitPoint[0].X >= limitPoint[4].X && limitPoint[0].Y < limitPoint[4].Y)
            {
                rect = new RectangleF(limitPoint[4].X, limitPoint[0].Y,
                limitPoint[0].X - limitPoint[4].X, limitPoint[4].Y - limitPoint[0].Y);
            }
            if (limitPoint[0].X >= limitPoint[4].X && limitPoint[0].Y >= limitPoint[4].Y)
            {
                rect = new RectangleF(limitPoint[4].X, limitPoint[4].Y,
                limitPoint[0].X - limitPoint[4].X, limitPoint[0].Y - limitPoint[4].Y);
            }

            gra.AddRectangle(rect);// gán rect cho gra
            return gra;
        }

        public void DrawLimitPoint(Graphics e)// vẽ 8 điểm giới hạn của tấm Pane
        {
            SolidBrush myBrush = new SolidBrush(Color.White);// 8 ô vuông đánh dấu 8 điểm giới hạn có nền màu trắng
            for (int i = 0; i < limitPoint.Length; i++)
            {
                e.FillRectangle(myBrush, limitPoint[i].X - 4, limitPoint[i].Y - 4, 8, 8);
                e.DrawRectangle(new Pen(Color.Black, 2), limitPoint[i].X - 4, limitPoint[i].Y - 4, 8, 8);// kích thước 8x8
            }
        }
    }
}
