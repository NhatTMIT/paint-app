﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp.Class
{
    public class BezierCurve : Shape, Handler
    {
        

        public BezierCurve()// khởi tạo giá trị
        {
            TYPE = TYPEMEMBER.BEZIERCURVE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
        }

        public BezierCurve(PointF point1, Color borderColor, Color layerColor, int borderwidth)// khởi tạo với điểm đầu, màu viền, màu nền, kích thước viền
        {
            TYPE = TYPEMEMBER.BEZIERCURVE;
            CornerRotate = 0;
            CenterPoint = new PointF();
            PointList = new List<PointF>();
            GroundPane = new Pane();
            PointList.Add(point1);
            GroundPane.FindLimit(PointList);

            BorderWidth = borderwidth;
            BorderColor = borderColor;
            BackGroundColor = layerColor;
        }

        public BezierCurve(Shape shape)// gán giá trị của class cha Shape lại cho BezierCurve
        {
            TYPE = TYPEMEMBER.BEZIERCURVE;
            this.CornerRotate = shape.CornerRotate;
            this.PointList = shape.PointList;
            this.BorderWidth = shape.BorderWidth;
            this.BorderColor = shape.BorderColor;
            this.BackGroundColor = shape.BackGroundColor;
            this.CenterPoint = shape.CenterPoint;
            this.GroundPane = shape.GroundPane;
        }

        public GraphicsPath DrawPath(List<PointF> points)// tạo GraphicPath cho hình BezierCurve với các points
        {
            GraphicsPath grap = new GraphicsPath();// khởi tạo GraphicsPath
            //PointF[] temp = new PointF[PointList.Count - 2];
            //PointList.CopyTo(1, temp, 0, PointList.Count - 2);
            grap.AddBeziers(PointList.ToArray());// Add Beziers cho GraphicsPath
            return grap;
        }

        public void SetCenterPoint()// tìm điểm trung tâm của hình dựa trên 4 góc của tấm GroundPane
        {
            GroundPane.FindLimit(PointList);// Xác định 8 điểm giới hạn của GroundPane

            // lấy trung điểm đường chéo của tấm Pane
            CenterPoint = new PointF((GroundPane.limitPoint[0].X + GroundPane.limitPoint[4].X) / 2,
                (GroundPane.limitPoint[0].Y + GroundPane.limitPoint[4].Y) / 2);
        }

        public Graphics Draw(Graphics g)// vẽ lên Graphic hình BezierCurve
        {
            // khởi tạo SolidBrush, Pen để tô màu và vẽ đường viền cho hình
            SolidBrush BackGroundSB = new SolidBrush(BackGroundColor);
            Pen BorderPen = new Pen(BorderColor, BorderWidth);

            Matrix myMatrix = new Matrix();            // tạo không gian ma trận
            SetCenterPoint();          // tìm điểm trung tâm hình
            myMatrix.RotateAt(CornerRotate, CenterPoint);// xoay hình theo không gian ma trận lấy tâm là CenterPoint

            //Rotate(CornerRotate);
            GroundPane.FindLimit(PointList);// xác định lại 8 điểm giới hạn cho GroundPane
            g.Transform = myMatrix;

            g.FillPath(BackGroundSB, DrawPath(PointList));// tô màu theo Path Bezier
            g.DrawPath(BorderPen, DrawPath(PointList));// vẽ đường viền cho hình
            if (IsSelected == true)// nếu hình được click vào nằm trong phạm vi GroundPane
            {
               
                float[] dashValues = { 5, 5 };
                Pen pen = new Pen(Color.Black, 1);
                pen.DashPattern = dashValues;
                g.DrawPath(pen, GroundPane.DrawPane()); // vẽ đường viền GroundPane
                GroundPane.DrawLimitPoint(g);// Vẽ 8 điểm của GroundPane
                DrawPoint(g);// vẽ điểm của hình Bezier
            }
            return g;
        }

        public void Zoom(float zoomIndex)// Zoom hình ra
        {
            for (int i = 0; i < PointList.Count(); i++)
            {
                PointList[i] = new PointF((PointList[i].X) * zoomIndex,
                    (PointList[i].Y) * zoomIndex);// nhân các điểm Point của hình cho hệ số nhân
            }
        }

        public void Translate(float[] target)// Tịnh tiến hình
        {
            for (int i = 0; i < PointList.Count(); i++)// cộng point cho vector tịnh tiến
                PointList[i] = new PointF(PointList[i].X + target[0], PointList[i].Y + target[1]);
            SetCenterPoint();// cập nhật lại centerPoint
        }

        public void Shear(float[] target)
        {
            for (int i = 0; i < PointList.Count(); i++)
                PointList[i] = new PointF(PointList[i].X * target[0], PointList[i].Y * target[1]);
            SetCenterPoint();
        }

        public void DrawPoint(Graphics e)// vẽ các điểm của hình Bezier
        {
            foreach (PointF p in PointList)
                e.FillEllipse(new SolidBrush(Color.Blue), p.X - 5, p.Y - 5, 10, 10);
        }

        public bool checkSelected(PointF p)
        {
            return DrawPath(PointList).IsVisible(p);
        }

    }
}
