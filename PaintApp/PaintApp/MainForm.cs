﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaintApp.Business;
using PaintApp.Class;

namespace PaintApp
{
    public partial class MainForm : Form
    {
        DrawingTable drawingTable;

        public MainForm()
        {
            InitializeComponent();
            drawingTable = new DrawingTable(panelDraw);
            drawingTable.ShowInfoEvent += showInfoEvent;
            drawingTable.ChangeOptionEvent += changeOptionEvent;
            drawingTable.Option = DrawingTable.OptionTool.Line;
            drawingTable.ShapeBackgroundColor = Color.White;
            drawingTable.ShapeBorderColor = Color.Black;
            drawingTable.BorderWidth = 3;
        }

        private void btn_editColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
        }

        private void panelDraw_MouseDown(object sender, MouseEventArgs e)
        {
            drawingTable.mouseDown(e);
        }

        private void panelDraw_MouseMove(object sender, MouseEventArgs e)
        {
            drawingTable.mouseMove(e);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //deskWork = new DeskWork(panel1.CreateGraphics() , panel1.Size);
            //deskWork.Option = DeskWork.OptionTool.Line;
        }

        private void panelDraw_MouseUp(object sender, MouseEventArgs e)
        {
            drawingTable.mouseUp(e);
        }

        private void panelDraw_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            drawingTable.mouseDoubleClick(e);
        }

        private void btnPencil_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Pen;
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Line;
        }

        private void btnEllipse_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Eclipse;
        }

        private void btnPolygon_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Polygon;
        }

        private void btnRectangle_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Rectangle;
        }

        private void btnCurve_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.BezierCurve;
        }

        private void btnSelectTool_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Select;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            drawingTable.saveFile();
        }

        private void panelDraw_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void btnManifer_Click(object sender, EventArgs e)
        {
            drawingTable.Option = DrawingTable.OptionTool.Zoom;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            drawingTable.readFile();
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            drawingTable.saveAsFile();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (drawingTable.newFile())
            {
                drawingTable = new DrawingTable(panelDraw);
                drawingTable.ShowInfoEvent += showInfoEvent;
                drawingTable.ChangeOptionEvent += changeOptionEvent;
                drawingTable.Option = DrawingTable.OptionTool.Line;
                drawingTable.ShapeBackgroundColor = Color.White;
                drawingTable.ShapeBorderColor = Color.Black;
                drawingTable.BorderWidth = 3;
                txtSize.TextBoxText = "3";
                btnBackGroundColor.BackColor = Color.White;
                btnBorderColor.BackColor = Color.Black;
            }
        }

        private void showInfoEvent(Shape shape)
        {
            if (shape != null)
            {
                panel3.Enabled = true;
                lbShapeName.Text = shape.TYPE.ToString();
                txtWidth.Text = (int)(Math.Abs(shape.GroundPane.limitPoint[0].X - shape.GroundPane.limitPoint[4].X) / drawingTable.CurrentZoomIndex) + "";
                txtHeight.Text = (int)(Math.Abs(shape.GroundPane.limitPoint[0].Y - shape.GroundPane.limitPoint[4].Y) / drawingTable.CurrentZoomIndex) + "";
                txtBorderWidth.Text = (shape.BorderWidth / drawingTable.CurrentZoomIndex) + "";
                txtRotate.Text = shape.CornerRotate + "";
                btnBackGroundColor.BackColor = shape.BackGroundColor;
                btnBorderColor.BackColor = shape.BorderColor;
            }
            else
            {
                panel3.Enabled = false;
                lbShapeName.Text = "";
                txtWidth.Text = "";
                txtHeight.Text = "";
                txtBorderWidth.Text = "";
                txtRotate.Text = "";
                btnBackGroundColor.BackColor = Color.White;
                btnBorderColor.BackColor = Color.White;
            }
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            string actualdata = string.Empty;
            char[] entereddata = txt.Text.ToCharArray();
            foreach (char aChar in entereddata.AsEnumerable())
            {
                if (Char.IsDigit(aChar))
                {
                    actualdata = actualdata + aChar;
                }
                else
                {
                    MessageBox.Show(aChar + " is not numeric");
                    actualdata.Replace(aChar, ' ');
                    actualdata.Trim();
                }
            }
            txt.Text = actualdata;
        }

        private void txtWidth_KeyDown(object sender, KeyEventArgs e)
        {
            Shape shape = drawingTable.SelectedShape;
            if (e.KeyCode == Keys.Enter)
            {
                int width = Int16.Parse(txtWidth.Text);
                float previousLocationX = shape.PointList[0].X;
                float previousWidth = (Math.Abs(shape.GroundPane.limitPoint[0].X - shape.GroundPane.limitPoint[4].X) / drawingTable.CurrentZoomIndex);
                float shearWidth = width / previousWidth ;
                ((Handler)shape).Shear(new float[2] { shearWidth, 1 });
                ((Handler)shape).Translate(new float[2] { previousLocationX - shape.PointList[0].X , 0 });
                drawingTable.drawLayer();
            }
        }

        private void txtHeight_KeyDown(object sender, KeyEventArgs e)
        {
            Shape shape = drawingTable.SelectedShape;
            if (e.KeyCode == Keys.Enter)
            {
                int height = Int16.Parse(txtHeight.Text);
                float previousLocationY = shape.PointList[0].Y;
                float previousHeight = (Math.Abs(shape.GroundPane.limitPoint[0].Y - shape.GroundPane.limitPoint[4].Y) / drawingTable.CurrentZoomIndex);
                float shearHeight = height / previousHeight;
                ((Handler)shape).Shear(new float[2] { 1, shearHeight });
                ((Handler)shape).Translate(new float[2] { 0, previousLocationY - shape.PointList[0].Y });
                drawingTable.drawLayer();
            }
        }

        private void txtBorderWidth_KeyDown(object sender, KeyEventArgs e)
        {
            Shape shape = drawingTable.SelectedShape;
            if (e.KeyCode == Keys.Enter)
            {
                shape.BorderWidth = Int16.Parse(txtBorderWidth.Text);
                drawingTable.drawLayer();
            }
        }

        private void txtRotate_KeyDown(object sender, KeyEventArgs e)
        {
            Shape shape = drawingTable.SelectedShape;
            if (e.KeyCode == Keys.Enter)
            {
                shape.CornerRotate = Int16.Parse(txtRotate.Text);
                drawingTable.drawLayer();
            }
        }

        private void btnBackGroundColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.AllowFullOpen = false;
            colorDialog.ShowHelp = true;
            colorDialog.Color = drawingTable.SelectedShape.BackGroundColor;
            
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                drawingTable.SelectedShape.BackGroundColor = colorDialog.Color;
                drawingTable.drawLayer();
            }
        }

        private void btnBorderColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.AllowFullOpen = false;
            colorDialog.ShowHelp = true;
            colorDialog.Color = drawingTable.SelectedShape.BorderColor;
           
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                drawingTable.SelectedShape.BorderColor = colorDialog.Color;
                drawingTable.drawLayer();
            }
        }

        private void panel2_Resize(object sender, EventArgs e)
        {
            drawingTable.drawLayer();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            drawingTable.drawLayer();
        }
        
        private void btnColor1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            btnColor1.Color = colorDialog1.Color;
            drawingTable.ShapeBackgroundColor = colorDialog1.Color;
        }

        private void btnColor2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            btnColor2.Color = colorDialog1.Color;
            drawingTable.ShapeBorderColor = colorDialog1.Color;
        }

        private void ribbonTextBox1_TextBoxTextChanged(object sender, EventArgs e)
        {
            RibbonTextBox txt = (RibbonTextBox)sender;
            char[] entereddata =txt.TextBoxText.ToCharArray();
            foreach (char aChar in entereddata.AsEnumerable())
            {
                if (!Char.IsDigit(aChar))
                {
                    txt.TextBoxText = "0";
                }
            }
        }

        private void ribbonTextBox1_TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            Shape shape = drawingTable.SelectedShape;
            if (e.KeyCode == Keys.Enter)
            {
                drawingTable.BorderWidth = Int16.Parse(txtSize.TextBoxText);
                drawingTable.drawLayer();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            drawingTable.newFile();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void changeOptionEvent(DrawingTable.OptionTool option)
        {
            switch (option)
            {
                case DrawingTable.OptionTool.Select:
                    btnSelectTool.Checked = true;
                    btnLine.Checked = false;
                    btnPolygon.Checked = false;
                    btnRectangle.Checked = false;
                    btnCurve.Checked = false;
                    btnEllipse.Checked = false;
                    btnPencil.Checked = false;
                    break;
                case DrawingTable.OptionTool.Line:
                    btnLine.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
                case DrawingTable.OptionTool.Polygon:
                    btnPolygon.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
                case DrawingTable.OptionTool.Rectangle:
                    btnRectangle.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
                case DrawingTable.OptionTool.BezierCurve:
                    btnCurve.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
                case DrawingTable.OptionTool.Eclipse:
                    btnEllipse.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
                case DrawingTable.OptionTool.Pen:
                    btnPencil.Checked = true;
                    btnSelectTool.Checked = false;
                    break;
            }
        }

    }
}
